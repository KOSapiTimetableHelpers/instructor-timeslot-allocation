/**
 * This class is currently work in progress. DO NOT USE IT.
 */
package cz.cvut.fit.instructor_timeslot_allocation;

import java.time.DayOfWeek;
import java.util.Collection;
import java.util.function.BooleanSupplier;
import java.util.function.Predicate;

public class Main {
//    features
//    (nice to have) load scheduled timetable slots from KOSapi
//    (must have) read teachers constraints
//    TODO which constraints: workload (min, max), co-teacher wishes, availability: when, how many timeslots in a row, how many days weekly, when breaks
//    TODO way how to enter/read the constraints; lambda-expressions?
//    TODO teachers constraints prioritized
//    (must have) allocate teachers to existing timeslots when respecting their constraints
    public static void main(String[] args) {
        /**
         * Scheduled time slot without teacher(s).
         */
        record TimetableSlot(DayOfWeek day, int number, int length, int teachers) {}
        /**
         * Single constraint of an instructor. allocTimetableEval takes complete allocation of all instructors.
         */
        record InstructorConstraint(int niceness, BooleanSupplier allocTimetableEval) {};
        record Instructor(String username, Collection<InstructorConstraint> constraints) {};
        /**
         * Time slot with planned teachers.
         */
        record AllocatedTimetableSlot(TimetableSlot ts, Instructor i1, Instructor i2) {};
        //TODO collection of all teachers (with constraints) and of all TimetableSlots
        //allocate teachers, evaluate all their constraints; if some constraints fail, backtrack and try different allocation
    }
}
